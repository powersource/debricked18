#include <stdio.h>
#include <stdlib.h>

void print_flag()
{
	char key[64];
	
	FILE *fp = fopen("flag.txt", "r");
	if (!fp) {
		printf("Error: Could not read flag. If you are running this remotely, contact an admin.\n");
		exit(1);
	}
	fscanf(fp, "%s", key);
	printf("%s\n", key);
	fclose(fp);
}

void vuln()
{
	char input[31];

	puts("Enter message:");
	gets(input);
}

int main()
{
	setvbuf(stdout, NULL, _IONBF, 0);

	vuln();
	return 0;
}
