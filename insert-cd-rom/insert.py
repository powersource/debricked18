import sha
from converter import convert

s = '803842140aac1bd12b36ec231e352a79e4e936e9'

for i in xrange(255**6): # 255**5 to 255**6
    h = sha.new()
    hx = convert('int', 'hex',i)
    #print hx
    h.update(hx)
    if i%(255**3) == 0:
        print "we're at", i
    if h.hexdigest() == s:
        print h.hexdigest()
