
async function sha512 (str, iterations) {
    let buffer = new TextEncoder('utf-8').encode(str)
  
    // Now apply WebCrypto digest by running SHA-512 for several iterations.
    for (let i = 0; i < iterations; i++) {
      buffer = await crypto.subtle.digest('SHA-512', buffer)
    }
    return hex(buffer)
  }
  
  function hex (buffer) {
    // Just convert a buffer to a hex string.
    let hexCodes = []
    let view = new DataView(buffer)
    for (let i = 0; i < view.byteLength; i += 4) {
      // Using getUint32 reduces the number of iterations needed (we process 4 bytes each time)
      let value = view.getUint32(i)
      // toString(16) will give the hex representation of the number without padding
      let stringValue = value.toString(16)
      // We use concatenation and slice for padding
      let padding = '00000000'
      let paddedValue = (padding + stringValue).slice(-padding.length)
      hexCodes.push(paddedValue)
    }
  
    // Join all the hex strings into one
    return hexCodes.join('')
  }
  
  async function checkPassword (username, password) {
    // Validate the login by using a very secure iterative SHA-512 with many iterations!
  
    // Set status to "hashing in progress"
    const alerts = document.getElementById('alerts')
    const alert = document.createElement('div')
    alert.textContent = 'Password hashing in progress...'
    alert.className = 'alert alert-info'
    alerts.innerHTML = ''
    alerts.appendChild(alert)
  
    // Hash a lot of times!
    const hash = await sha512(password, 100000)
  
    const validHash = '3790a3454f9cd26fbe2392a1ef99c10f10fc246c7fe5667966475c741f38eec85302e228b1a89de4c0b88c5ea9604dcf654fae8c138af5082d84241b4411eb98'
    const validUsername = 'admin'
  
    // Notify server of result.
    const request = {
      passwordOk: (hash === validHash && username === validUsername)
    }
  
    const response = await fetch('/validate', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(request)
    })
    if (response.status === 200) {
      const jsonResponse = await response.json()
      // Login successful! Print flag in alert.
      const alerts = document.getElementById('alerts')
      const alert = document.createElement('div')
      alert.textContent = 'You are logged in. The flag is: ' + jsonResponse.flag
      alert.className = 'alert alert-success'
      alerts.innerHTML = ''
      alerts.appendChild(alert)
    } else {
      // Login failed.
      const alerts = document.getElementById('alerts')
      const alert = document.createElement('div')
      alert.textContent = 'Login failed. Username or password is incorrect.'
      alert.className = 'alert alert-danger'
      alerts.innerHTML = ''
      alerts.appendChild(alert)
    }
  }
  
  function processForm (e) {
    if (e.preventDefault) {
      e.preventDefault()
    }
  
    const username = document.getElementById('inputEmail').value
    const password = document.getElementById('inputPassword').value
    checkPassword(username, password)
    return false
  }
  
  const form = document.getElementById('superform')
  form.addEventListener('submit', processForm)
  