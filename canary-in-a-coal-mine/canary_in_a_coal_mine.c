#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define BUFSIZE 32
#define CANARY_SIZE 4

void print_flag()
{
	char flag[64];
	
	FILE *fp = fopen("flag.txt", "r");
	if (!fp) {
		printf("Error: Could not read flag. If you are running this remotely, contact an admin.\n");
		exit(1);
	}
	fscanf(fp, "%s", flag);
	printf("%s\n", flag);
	fclose(fp);
}

char global_canary[4];
void read_canary()
{
	FILE *fp = fopen("canary.txt", "r");
	if (!fp) {
		printf("Error: Could not read canary. If you are running this remotely, contact an admin.\n");
		exit(1);
	}
	
	fread(global_canary, 1, CANARY_SIZE, fp);
	fclose(fp);
}

void vuln()
{
	char canary[CANARY_SIZE];
	char buf[BUFSIZE];
	char length[BUFSIZE];
	int count;
	int x = 0;
	memcpy(canary, global_canary, CANARY_SIZE);
	printf("How many bytes will you write into the buffer?\n> ");
	while (x < BUFSIZE) {
		read(0, length + x, 1);
		if (length[x] == '\n') break;
		x++;
	}
	sscanf(length, "%d", &count);

	printf("Input> ");
	read(0, buf, count);

	if (memcmp(canary, global_canary, CANARY_SIZE)) {
		printf("*** Stack Smashing Detected *** : Canary Value Corrupt!\n");
		exit(-1);
	}
	printf("Ok... Now where's the flag?\n");
	fflush(stdout);
}

int main()
{
	setvbuf(stdout, NULL, _IONBF, 0);

	read_canary();
	vuln();
	return 0;
}
